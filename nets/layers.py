import json

import numpy as np
from onnx import load_model, shape_inference
from onnx_tf.backend import prepare
import keras
import tensorflow as tf
name = "resnet18-v1-7"

import onnx
#onnx_model = onnx.load(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/{name}.onnx")
#tf_rep = prepare(onnx_model)

#imported = keras.models.load_model(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/tp.pb")
#print(imported)
 
#onnx_model = onnx.load(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/{name}.onnx")
#tf_rep = prepare(onnx_model)
#tf_rep.export_graph(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/{name}.pb")



from onnx2pytorch import ConvertModel

onnx_model = onnx.load(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/{name}.onnx")
pytorch_model = ConvertModel(onnx_model)