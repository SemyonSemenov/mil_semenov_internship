import json

import numpy as np
from onnx import load_model, shape_inference


class ONNXParser:
    def __init__(self,
                 onnx_path: str,
                 verbose: bool = False):
        onnx_model = load_model(onnx_path)
        onnx_inference_model = shape_inference.infer_shapes(onnx_model)

        self.verbose = verbose
        self.node = onnx_inference_model.graph.node
        self.value_info = onnx_inference_model.graph.value_info
        self.n = len(self.value_info)

        self.adj_matrix = [[0 for _ in range(self.n)] for _ in range(self.n)]
        self.model_dict = []
        self.layers = {}

        self._parse()

    def _parse(self):
        for i in range(self.n):
            json_dim = self.value_info[i].type.tensor_type.shape.dim
            dim = [i.dim_value for i in json_dim]
            self.layers[self.value_info[i].name] = {
                "output_shape": dim,
                "index": i
            }

        op_types = {}
        for i in range(len(self.node)):
            # create an operation type dictionary
            # to encode the type of the output layer:
            op_type_name = self.node[i].op_type
            if op_type_name not in op_types.keys():
                op_types[op_type_name] = len(op_types)

            input_name = self.node[i].input[0]
            output_name = self.node[i].output[0]

            layers_keys = list(self.layers.keys())
            if input_name in layers_keys and output_name in layers_keys:
                input_index = self.layers[input_name]["index"]
                output_index = self.layers[output_name]["index"]
                self.adj_matrix[input_index][output_index] = 1

                self.layers[output_name]["input_shape"] = \
                    self.layers[input_name]["output_shape"]
            elif input_name not in layers_keys and output_name in layers_keys:
                # i.e. input_tensor:0 > resnet_model/conv2d/Conv2D:0
                # so there is no information about input's input shape
                self.layers[output_name]["input_shape"] = \
                    self.layers[output_name]["output_shape"]
            elif input_name in layers_keys and output_name not in layers_keys:
                # i.e. raw_output___5:0 > softmax_tensor:0
                # so there is no information about output's output shape
                self.layers[input_name]["output_shape"] = \
                    self.layers[input_name]["input_shape"]

            if output_name in list(self.layers.keys()):
                self.layers[output_name]["op_type"] = op_types[op_type_name]

            if self.verbose:
                if input_name in layers_keys:
                    print(f"{input_name}: "
                          f"{self.layers[input_name]['input_shape']}"
                          f" > {self.layers[input_name]['output_shape']}")
                else:
                    print("nothing")
                print(">>>")
                if output_name in layers_keys:
                    print(f"{output_name}: "
                          f"{self.layers[output_name]['input_shape']}"
                          f" > {self.layers[output_name]['output_shape']}")
                else:
                    print("nothing")
                print("-" * 50)

        for layer in self.layers.values():
            self.model_dict.append({
                "op_type": layer["op_type"],
                "input_shape": layer["input_shape"],
                "output_shape": layer["output_shape"]
            })

    def to_json(self, json_path: str):
        with open(json_path, "w") as file:
            json.dump({
                "adj_matrix": self.adj_matrix,
                "model_dict": self.model_dict
            }, file)


if __name__ == "__main__":
    name = "resnet50_beheaded"
    parser = ONNXParser(f"../nets/{name}.onnx", verbose=False)
    parser.to_json(f"../nets/{name}.json")
    adj_matrix = np.array(parser.adj_matrix)
    model_dict = np.array(parser.model_dict)
    print(adj_matrix.shape, model_dict.shape)
    print(adj_matrix)
    print(model_dict)