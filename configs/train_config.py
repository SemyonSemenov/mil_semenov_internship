"""
In DummyGraphEnv we want to do only 1 step. Since stable_baselines_3
the minimum number of steps (n_steps) is 2.

:param log_path: The path to the directory where the training logs are
saved.
:type log_path: str

:param agent_weights_path: The path to the directory where the weights
of the agent are saved.
:type agent_weights_path: str

:param n_steps: Number of iterations in the environment for 1 epoch of
training.
:type n_steps: int

:param batch_size: Batch size. Must be a multiple of 'n_steps * n_envs'.
Since the environment is not vectorized, n_env = 1.
:type batch_size: int

:param n_iter: Number of training iterations.
:type n_iter: int

:param environment: The name of the environment class from the
environment.
:type environment: str

:param callback: Callback class name from
custom_stable_baselines_3.callbacks.
:type callback: str

:param callback_kwargs: Dictionary of named arguments for callback.
:type callback_kwargs: dict

:param features_extractor_class: The name of the environment class from
custom_stable_baselines_3.features_extractors.
:type features_extractor_class: str

:param features_extractor_kwargs: Dictionary of named arguments for
features_extractor.
:type features_extractor_kwargs: dict

:param device: Dictionary of named arguments for
features_extractor.
:param device: dict

:param device: Name of the device on which the agent is running.
:type device: str
"""

train_config = {
    'log_path': "./training_logs",
    'agent_weights_path': "./saved_model",
    'n_steps': 2,
    'batch_size': 2,
    'n_iter': 50,
    'environment': 'DummyGraphEnv',
    'callback': 'GraphSageEmbeddingCallback',
    'callback_kwargs': {
        'directory_path': "./embeddings"
    },
    'features_extractor_class': 'DummyCombinedExtractor',
    'features_extractor_kwargs': {},
    'device': 'cpu'
    # n_steps = 2,  batch_size = 2, n_iter = 50
}
