from .train_config import train_config

"""
The MlpExtractor configuration file.

Number of elements inside aggregator_hidden_units must match the number
of elements inside encoder_out_features.

Required arguments:
* mlp_extractor_name
* skip_inputs: [], if not exists;
* in_features: None, if not exists;
* latent_dim_pi;
* latent_dim_vf.

:param mlp_extractor_name: The name of the class inside
custom_stable_baselines_3.mlp_extractors.
:type mlp_extractor_name: str

:param skip_inputs: List of observations which are not feature.
:type skip_inputs: List[str]

:param in_features: Input dimension of feature. If None - calculated
based on the names of observations and the skip_inputs argument
inside mlp_extractors.
:type in_features: int

:param aggregator_hidden_units: List of output dimensions of 
aggregators.
:type aggregator_hidden_units: List[int]

:param encoder_out_features: List of output dimensions of encoders.
:type encoder_out_features: List[int]

:param hidden_features: The number of neurons in the hidden layer after 
GraphSAGE.
:type hidden_features: int

:param use_dropout: Flag of the use of the dropout layer between 
encoders.
:type use_dropout: bool

:param dropout_rate: Probability of dropout. Relevant when 
use_dropout = True.
:type dropout_rate: float

:param activation: The name of the activation function class from 
torch.nn.
:type activation: str

:param device: Name of the device on which the agent is running. Pulled 
from the training configuration.
:type device: str

:param latent_dim_pi: The number of neurons in the layer before the 
output of actions.
:type latent_dim_pi: int

:param latent_dim_vf: The number of neurons in the layer before the
output the state_value.
:type latent_dim_vf: int
"""

mlp_extractor_config = {
    'mlp_extractor_name': 'GraphSAGEMlpExtractor',
    'skip_inputs': ['adjacency_matrix', ],
    'in_features': None,
    'aggregator_hidden_units': [4, 4],
    'encoder_out_features': [4, 4],
    'hidden_features': 64,
    'use_dropout': True,
    'dropout_rate': 0.1,
    'activation': 'ReLU',
    'device': train_config['device'],
    'latent_dim_pi': 64,
    'latent_dim_vf': 64,
}
