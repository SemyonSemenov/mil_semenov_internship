from typing import Tuple

import gym
import numpy as np
#from runner_hetero.runner import DeviceType

from graph_rl.data import Data
from graph_rl.utils import get_reward, invert_list_of_dicts, \
    InfinitePlacementError

from configs.train_config import train_config

class DummyGraphEnv(gym.Env, Data):
    """
    Environment, with no access to information about the state of the
    hardware.

    Observations:
    1) Type of node operation;
    2) Input dimensionality to the node;
    3) Output dimensionality of a node;
    4) Node adjacency matrix.

    The episode lasts 1 step, for which the whole graph is plotted.
    Reward counts as best_runtime - runtime.
    The default placement is the placement on the NPU.
    """

    def __init__(self, config):
        """
        :param config: Environment configuration. Present for code
        compatibility.
        :type config: dict
        """
        Data.__init__(self)
        self.default_placement = np.full(self.NUM_NODES, fill_value=0)
        self.default_runtime = get_reward(self.default_placement)
        self.best_runtime = None
        self.state = None

        self._setup_spaces()
        self.reset()
        self.save_path = '/home/semenov/Projects/huawei_dnn_graph_optimisers/time/'
        self.times = []

    def _setup_spaces(self):
        shape_dims = len(self.MAX_IN_SHAPE)
        spaces = {
            'op_type':
                gym.spaces.Box(low=0,
                               high=self.NUM_OPS,
                               shape=(self.NUM_NODES, 1),
                               dtype=np.int64),
            'input_shape':
                gym.spaces.Box(low=1,
                               high=max(self.MAX_IN_SHAPE),
                               shape=(self.NUM_NODES, shape_dims),
                               dtype=np.int64),
            'output_shape':
                gym.spaces.Box(low=1,
                               high=max(self.MAX_OUT_SHAPE),
                               shape=(self.NUM_NODES, shape_dims),
                               dtype=np.int64),
            'adjacency_matrix':
                gym.spaces.Box(low=0,
                               high=1,
                               shape=(self.NUM_NODES, self.NUM_NODES),
                               dtype=np.int64),
        }
        self.observation_space = gym.spaces.Dict(spaces)
        self.action_space = gym.spaces.MultiDiscrete(
            [self.NUM_DEVICES] * self.NUM_NODES)

    def reset(self):
        
        self.best_runtime = self.default_runtime
        # self.state = invert_list_of_dicts(self.data,
        #                                   keys=['op_type', 'input_shape',
        #                                         'output_shape'])
        self.state = self.data.copy()
        self.state['op_type'] = self.state['op_type'].reshape(-1, 1)
        self.state['adjacency_matrix'] = self.adj_matrix
        return self.state

    def step(self, action: np.ndarray) -> Tuple[dict, float, bool, dict]:
        """
        :param action: Vector id of the devices on which you want to
        place model layers.
        :type action: np.ndarray

        :return: Observation, reward, episode completion flag,
        additional information.
        :rtype: Tuple(dict, float, bool, dict)
        """
        self.default_runtime = get_reward(self.default_placement)
        runtime = get_reward(action)
        print('self.times: ', self.times)
        self.times += [runtime]
        if runtime == np.inf:
            raise InfinitePlacementError('Infinite placement error')
        reward = self.default_runtime - runtime
        if runtime < self.best_runtime:
            self.best_runtime = runtime
        print(f'Runtime - {runtime}')
        print('Best runtime: {self.best_runtime}')
        print(f'Reward: {reward}\nDevice placement: {action}\n\n')

        if len(self.times) == train_config['batch_size']:
            with open(f'{self.save_path}/mean_time.txt', 'a') as f:
                f.write(str(np.mean(self.times)) + '\n')
                f.close()
            with open(f'{self.save_path}/min_time.txt', 'a') as f:
                f.write(str(np.min(self.times)) + '\n')
                f.close()
            with open(f'{self.save_path}/default_time.txt', 'a') as f:
                f.write(str(get_reward(self.default_placement)) + '\n')
                f.close()
            self.times = []

        done = True
        return self.state, reward, done, {}
