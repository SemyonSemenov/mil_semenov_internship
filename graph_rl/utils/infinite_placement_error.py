class InfinitePlacementError(Exception):
    """
    Класс ошибки, которая вызывается при плейсменте, вернувшем время inf.
    """
    pass
