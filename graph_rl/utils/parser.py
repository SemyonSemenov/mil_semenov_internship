"""
The ONNX file parser.
"""

import json
import os

import numpy as np
from onnx import load_model, shape_inference


class ONNXParser:
    """
    A class to parse onnx models and get adjacency matrix of the model
    graph and information about layers: operation type (encoded),
    input and output dimensions.
    """

    def __init__(self, onnx_path, verbose=False):
        """
        Initialize a class that stores the information about the
        neural net.

        :param onnx_path: A string with the path to the onnx model
        :type onnx_path: str
        :param verbose: A bool to control whether to print debug info
        :type verbose: bool
        """
        self._check_is_file(onnx_path)

        onnx_model = load_model(onnx_path)
        onnx_inference_model = shape_inference.infer_shapes(onnx_model)

        self.verbose = verbose
        self.node = onnx_inference_model.graph.node
        self.value_info = onnx_inference_model.graph.value_info
        self.output = onnx_inference_model.graph.output
        self.n = len(self.value_info) + len(self.output)

        self.adj_matrix = np.zeros((self.n, self.n))
        self.model_dict = []
        self.layers = {}

        self._parse()

    def _check_is_file(self, path):
        """
        Check if the file exists.

        :param path: A path to the file to check if it exists
        :type path: str
        """
        assert os.path.isfile(path) and os.access(path, os.R_OK), \
            "Either the file is missing or not readable! " \
            f"File's here: {path}, you're here: {os.getcwd()}"

    def _parse(self):
        """
        Parse ONNX file and get layers dimensions and adjacency matrix
        of the neural net graph.
        """
        max_dim_len = 0
        for i in range(len(self.value_info)):
            json_dim = self.value_info[i].type.tensor_type.shape.dim
            dim = [i.dim_value for i in json_dim]
            if len(dim) > max_dim_len:
                max_dim_len = len(dim)
            self.layers[self.value_info[i].name] = {
                "output_shape": dim,
                "index": i
            }

        for i in range(len(self.output)):
            json_dim = self.output[i].type.tensor_type.shape.dim
            dim = [i.dim_value for i in json_dim]
            if len(dim) > max_dim_len:
                max_dim_len = len(dim)
            self.layers[self.output[i].name] = {
                "output_shape": dim,
                "index": i
            }

        op_types = {}
        for i in range(len(self.node)):
            # create an operation type dictionary
            # to encode the type of the output layer:
            op_type_name = self.node[i].op_type
            if op_type_name not in op_types.keys():
                op_types[op_type_name] = len(op_types)

            input_name = self.node[i].input[0]
            output_name = self.node[i].output[0]

            layers_keys = list(self.layers.keys())
            if input_name in layers_keys and output_name in layers_keys:
                input_index = self.layers[input_name]["index"]
                output_index = self.layers[output_name]["index"]
                self.adj_matrix[input_index][output_index] = 1

                self.layers[output_name]["input_shape"] = \
                    self.layers[input_name]["output_shape"]
            elif input_name not in layers_keys and output_name in layers_keys:
                # i.e. input_tensor:0 > resnet_model/conv2d/Conv2D:0
                # so there is no information about input's input shape
                self.layers[output_name]["input_shape"] = \
                    self.layers[output_name]["output_shape"]
            elif input_name in layers_keys and output_name not in layers_keys:
                # i.e. raw_output___5:0 > softmax_tensor:0
                # so there is no information about output's output shape
                self.layers[input_name]["output_shape"] = \
                    self.layers[input_name]["input_shape"]

            if output_name in list(self.layers.keys()):
                self.layers[output_name]["op_type"] = op_types[op_type_name]

            if self.verbose:
                if input_name in layers_keys:
                    print(f"{input_name}: "
                          f"{self.layers[input_name]['input_shape']}"
                          f" > {self.layers[input_name]['output_shape']}")
                else:
                    print("nothing")
                print(">>>")
                if output_name in layers_keys:
                    print(f"{output_name}: "
                          f"{self.layers[output_name]['input_shape']}"
                          f" > {self.layers[output_name]['output_shape']}")
                else:
                    print("nothing")
                print("-" * 50)

        for layer in self.layers.values():
            input_len = max_dim_len - len(layer["input_shape"])
            input_shape = np.pad(layer["input_shape"],
                                 (0, input_len),
                                 constant_values=1).tolist()

            output_len = max_dim_len - len(layer["output_shape"])
            output_shape = np.pad(layer["output_shape"],
                                  (0, output_len),
                                  constant_values=1).tolist()

            self.model_dict.append({
                "op_type": layer["op_type"],
                "input_shape": input_shape,
                "output_shape": output_shape
            })

    def to_json(self, json_path):
        """
        Save the model information as a json file.

        :param json_path: Where to save the json file
        :type json_path: str
        """
        with open(json_path, "w") as file:
            json.dump({
                "adj_matrix": self.adj_matrix.tolist(),
                "model_dict": self.model_dict
            }, file)
