import os
from collections import OrderedDict

import numpy as np
import torch


# make an onehot-encoding of the array of integer labels
def encode_onehot(labels):
    classes = set(labels)
    classes_dict = {
        c: np.identity(len(classes))[i, :]
        for i, c in enumerate(classes)
    }
    labels_onehot = np.array(list(map(classes_dict.get, labels)),
                             dtype=np.int32)
    return labels_onehot


# fix random seeds for results reproducibility
def fix_seeds(seed=42):
    torch.manual_seed(seed)
    np.random.seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


# print all entries in FLAGS variable
def print_flags(flags):
    print('\n' + '-' * 30)
    for key, value in vars(flags).items():
        print(key + ' : ' + str(value))
    print('-' * 30, '\n')


# from the list of dicts get a dict of lists
def invert_list_of_dicts(model_dict, keys):
    new_model_dict = OrderedDict()
    for k in keys:
        new_model_dict[k] = np.array(
            [model_dict[i][k] for i in range(len(model_dict))])
    return new_model_dict


# check if the path exists, if not create corresponding dir
def check_make(path):
    if not os.path.exists(path):
        os.mkdir(path)
