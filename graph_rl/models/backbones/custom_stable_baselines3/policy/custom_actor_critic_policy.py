from stable_baselines3.common.policies import ActorCriticPolicy

from configs import mlp_extractor_config
from graph_rl.models.backbones.custom_stable_baselines3 import mlp_extractors


class CustomActorCriticPolicy(ActorCriticPolicy):
    """
    Переопределение класс ActorCriticPolicy из StableBaselines3,
    чтобы использовать кастомный MlpExtractor.
    MlpExtractor и его аргументы определяются в файле конфигурации
    mlp_extractor_config.
    """

    def _build_mlp_extractor(self) -> None:
        """
        Метод, отвечающий за инициализацию MlpExtractor.
        Аргументы, необходимые для инициализации, находятся в файле
        конфигурации mlp_extractor_config.
        """
        mlp_extractor_kwargs = mlp_extractor_config.copy()

        skip_inputs = mlp_extractor_kwargs.pop('skip_inputs')
        if mlp_extractor_kwargs['in_features'] is None:
            spaces = self.observation_space.spaces
            input_shapes = []
            for k in spaces.keys():
                if k not in skip_inputs:
                    input_shapes.append(spaces[k].shape[-1])
            mlp_extractor_kwargs['in_features'] = sum(input_shapes)

        mlp_extractor_name = mlp_extractor_kwargs.pop('mlp_extractor_name')
        mlp_extractor_class = getattr(mlp_extractors, mlp_extractor_name)
        self.mlp_extractor = mlp_extractor_class(**mlp_extractor_kwargs)
