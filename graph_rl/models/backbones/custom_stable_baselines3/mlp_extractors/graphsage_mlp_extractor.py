from typing import List
# from typing_extensions import Required

import torch
import torch.nn as nn
from stable_baselines3.common.torch_layers import MlpExtractor

from graph_rl.data import Data
from graph_rl.models.backbones.layers.graph_sage import GraphSage
from graph_rl.models.backbones.layers.graph_sage import GraphSageSampling
from transformers import XLNetConfig, XLNetModel

class GraphSAGEMlpExtractor(MlpExtractor, Data):
    """
    Принцип работы модели:
    1) Данные об узлах распаковываются на признаки и матрицу смежности.
    2) Данные прогоняются через GraphSAGE для получения эмбеддингов узлов.
    3) На основе эмбеддингов узлов формируется скрытое представление state
    value и действий.
    """

    def __init__(self,
                 in_features: int,
                 aggregator_hidden_units: List[int],
                 encoder_out_features: List[int],
                 hidden_features: int,
                 use_dropout: bool = True,
                 dropout_rate: float = 0.3,
                 activation: str = 'ReLU',
                 device: str = 'cpu',
                 latent_dim_pi: int = 64,
                 latent_dim_vf: int = 64):
        nn.Module.__init__(self)
        Data.__init__(self)
        assertion_msg_1 = 'Aggregators number should match encoders number!'
        check_1 = len(aggregator_hidden_units) == len(encoder_out_features)
        assert check_1, assertion_msg_1
        assertion_msg_2 = 'Dropout is allowed only between encoders!'
        if use_dropout:
            check_2 = len(encoder_out_features) > 1
            assert check_2, assertion_msg_2

        self.in_features = in_features
        self.device = device
        # Параметры, необходимые для StableBaselines3
        self.hidden_features = hidden_features
        self.latent_dim_pi = latent_dim_pi
        self.latent_dim_vf = latent_dim_vf

        self.graphsage = GraphSageSampling(in_features,
                                   aggregator_hidden_units,
                                   encoder_out_features,
                                   activation,
                                   use_dropout,
                                   dropout_rate,
                                   device)                           

        self.hidden = nn.Linear(self.graphsage.out_features * self.NUM_NODES,
                                self.hidden_features,
                                device=device, bias=False)

        self.flatten = nn.Flatten()

        self.latent_vf = torch.nn.Linear(self.hidden_features,
                                   self.latent_dim_vf,
                                   device=device)
                                   
        self.latent_pi = nn.Linear(self.hidden_features,
                                   self.latent_dim_pi,
                                   device=device)

        self.activation = getattr(nn, activation)()

        self.configuration = XLNetConfig(d_model = self.graphsage.out_features, n_head = self.graphsage.out_features)
        self.transformer = XLNetModel(self.configuration)


    def unpack_observation(self, observation):
        op_types = observation['op_type']
        if op_types.device.type != self.device:
            op_types = op_types.to(self.device)

        output_shapes = observation['output_shape']
        if output_shapes.device.type != self.device:
            output_shapes = output_shapes.to(self.device)

        input_shapes = observation['input_shape']
        if input_shapes.device.type != self.device:
            input_shapes = input_shapes.to(self.device)

        adjacency_matrix = observation['adjacency_matrix']
        if adjacency_matrix.device.type != self.device:
            adjacency_matrix = adjacency_matrix.to(self.device)

        return op_types, output_shapes, input_shapes, adjacency_matrix

    def forward(self, observation: dict):
        unpacked = self.unpack_observation(observation)
        op_types, output_shapes, input_shapes, adjacency_matrix = unpacked

        nodes_data = torch.cat([op_types, input_shapes, output_shapes], dim=-1)
        embeddings = self.graphsage(nodes_data, adjacency_matrix)
        
        last_hidden_states = self.transformer(inputs_embeds = embeddings).last_hidden_state
        hidden = self.hidden(self.flatten(last_hidden_states))
        # hidden = self.hidden(self.flatten(embeddings))
        
        latent_vf = self.activation(self.latent_vf(hidden))
        latent_pi = self.activation(self.latent_pi(hidden))
        return latent_pi, latent_vf

    def forward_actor(self, features: torch.Tensor) -> torch.Tensor:
        latent_pi, _ = self.forward(features)
        return latent_pi

    def forward_critic(self, features: torch.Tensor) -> torch.Tensor:
        _, latent_vf = self.forward(features)
        return latent_vf
