import os

import torch as th
from stable_baselines3.common.callbacks import BaseCallback


class GraphSageEmbeddingCallback(BaseCallback):
    """
    Callback для сохранения в файл эмбеддингов GraphSAGE после каждой итерации
    обучения.
    """

    def __init__(self, directory_path: str, verbose=0):
        """
        :param directory_path: Путь до директории, в которую нужно сохранить.
        :type directory_path: str
        :param verbose: Verbose
        :type verbose: int
        """
        BaseCallback.__init__(self, verbose)

        self.iter = 0
        self.directory_path = directory_path
        if not os.path.exists(self.directory_path):
            os.mkdir(self.directory_path)

    def on_rollout_end(self) -> None:
        """
        Этот метод будет вызван перед обновлением Policy.
        Это позволяет сохранять эмбеддинги начиная с нулевой итерации обучения.
        """
        self._get_embeddings()
        self.iter += 1

    def on_training_end(self) -> None:
        """
        Этот метод будет вызван после завершения обучения.
        Это позволяет сохранить эмбеддинг с последней итерации обучения.
        """
        self._get_embeddings()

    def _save(self, tensor: th.Tensor, name: str) -> None:
        """
        Метод сохранения тензоров в файл

        :param tensor: Тезор, который нужно сохранить
        :type tensor: th.Tensor
        :param name: Название тензора
        :type name: str
        """
        path = os.path.join(self.directory_path, f'{name}_{self.iter}.pt')
        th.save(tensor, path)

    def _get_embeddings(self):
        """
        Метод для сбора эмбеддингов.
        Алгоритм действий:
        1) Распаковать наблюдения, собранные для текущей итерации обучения.
        2) Пропустить наблюдения через GraphSage.
        3) Сохранить входные данные и полученные эмбеддинги в файл .pt.
        """
        mlp_extractor = self.model.policy.mlp_extractor
        rollout_buffer = self.model.rollout_buffer
        embedding_storage = []
        for rollout_data in rollout_buffer.get(self.model.batch_size):
            observation = rollout_data.observations
            unpacked = mlp_extractor.unpack_observation(observation)
            op_types, output_shapes, input_shapes, adjacency_matrix = unpacked
            self._save(op_types, 'op_types')
            self._save(output_shapes, 'output_shapes')
            self._save(input_shapes, 'input_shapes')
            self._save(adjacency_matrix, 'adjacency_matrix')

            nodes_data = th.cat([op_types, input_shapes, output_shapes],
                                dim=-1)
            with th.no_grad():
                embeddings = mlp_extractor.graphsage(nodes_data,
                                                     adjacency_matrix)
            embedding_storage.append(embeddings)
        embeddings = th.cat(embedding_storage, dim=0)
        self._save(embeddings, 'embeddings')

    def _on_step(self) -> bool:
        """
        Отстутсвие переопределения этого метода ведет к ошибке.
        :rtype: bool
        """
        pass
