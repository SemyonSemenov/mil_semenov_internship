from typing import Dict

import gym
import torch as th
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor


class DummyCombinedExtractor(BaseFeaturesExtractor):
    """
    Класс FeatureExtractor, транслирующего вход на выход.
    Экзмемпляр данного класса обязателен в StableBaselines3, однако наш
    MlpExtractor в нем не нуждается.
    """

    def __init__(self, observation_space: gym.spaces.Dict):
        """
        Инициализация аргумента features_dim базового класса должна быть
        произведена любым числом, отличным от нуля. На данном этапе
        features_dim все равно не известно.
        
        :param observation_space: Пространство наблюдений среды.
        :type observation_space: gym.spaces.Dict
        """
        BaseFeaturesExtractor.__init__(self,
                                       observation_space,
                                       features_dim=1)

    def forward(self,
                observations: Dict[str, th.Tensor]) -> Dict[str, th.Tensor]:
        """
        :param observations: Именованные наблюдения для обучения агента.
        :type observations: Dict[str, th.Tensor]
        :return: Входные данные без изменений.
        :rtype: Dict[str, th.Tensor]
        """
        return observations
