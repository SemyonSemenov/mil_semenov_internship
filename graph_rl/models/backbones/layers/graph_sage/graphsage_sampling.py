from numbers import Number
from typing import List

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn.functional import softplus

from .aggregators import PoolingAggregator
from .encoder import Encoder
import numpy as np


class GraphSageSampling(nn.Module):
    """
    Данный GraphSAGE отличает от классического тем, что его выход сэмплируется на манер VAE.
    Т.к. GraphSAGE предсказывает эмбеддинги нод, а не графа, то и сэмплирование производится для
    каждой ноды.
    """

    def __init__(self,
                 in_features: int,
                 aggregator_hidden_units: List[int],
                 encoder_out_features: List[int],
                 activation: str,
                 use_dropout: bool = True,
                 dropout_rate: float = 0.3,
                 device: str = 'cpu'):
        super(GraphSageSampling, self).__init__()
        self.in_features = in_features
        self.out_features = encoder_out_features[-1]
        self.distrib_params_split_index = encoder_out_features[-1]
        self.device = device
        self.use_dropout = use_dropout
        if self.use_dropout:
            self.dropout_rate = dropout_rate
            self.dropout = nn.Dropout(p=self.dropout_rate)
        encoders = []
        for i in range(len(aggregator_hidden_units)):
            if i == 0:
                agg_in_features = in_features
            else:
                agg_in_features = encoder_out_features[i - 1]
            aggregator = PoolingAggregator(agg_in_features,
                                           aggregator_hidden_units[i],
                                           activation,
                                           self.device)
            if i == len(aggregator_hidden_units) - 1:
                # Последний энкодер возвращает mu и std распределения заданной размерности
                enc_out_features = encoder_out_features[i] * 2
            else:
                enc_out_features = encoder_out_features[i]
            encoder = Encoder(enc_out_features,
                              aggregator,
                              activation,
                              device)
            encoders.append(encoder)
        self.encoders = nn.ModuleList(encoders)

    def forward(self, nodes: torch.Tensor, adjacency_matrix: torch.Tensor) -> torch.Tensor:
        """
        nodes: [batch, num_nodes, features]
        neighbours: [batch, num_nodes, None]
        """
        batch_size = nodes.shape[0]
        num_nodes = nodes.shape[1]
        # Инициализация эмбеддингов значениями признаков
        embeddings = nodes
        # Обратить направление связей в графе
        adjacency_matrix = torch.transpose(adjacency_matrix, 1, 2)
        adjacency_matrix = adjacency_matrix.permute(1, 0, 2)
        for k in range(len(self.encoders)):
            out_features = self.encoders[k].out_features
            tmp = torch.empty(size=(num_nodes, batch_size, out_features), dtype=torch.float, device=self.device)
            for i, (node, neighbours_ids) in enumerate(zip(embeddings.permute(1, 0, 2), adjacency_matrix)):
                neighbours = self.get_neighbours(embeddings, neighbours_ids.unsqueeze(-1))
                node = self.encoders[k](node, neighbours)
                tmp[i] = node
            embeddings = tmp.permute(1, 0, 2)
            embeddings = embeddings / torch.norm(embeddings, dim=-1).unsqueeze(-1)
            if self.use_dropout and k < len(self.encoders) - 1:
                embeddings = self.dropout(embeddings)
            embeddings[torch.isnan(embeddings)] = 0.0
        # Возможная проблема: mu и std могут оказаться нулевыми из-за dropout
        mu = embeddings[:, :, :self.distrib_params_split_index]
        std = softplus(embeddings[:, :, self.distrib_params_split_index:] - 5, beta=1) + 1e-7
        # Размерность должна быть вида [batch, seq_len, emb_dim], где emb_dim - размерность выхода последнего энкодера
        if len(mu.shape) == 2:
            mu = mu.unsqueeze(-1)
        if len(std.shape) == 2:
            std = std.unsqueeze(-1)
        return self.reparametrize_n(mu, std, n=1)

    def get_neighbours(self, nodes: torch.Tensor, neighbours: torch.Tensor) -> torch.Tensor:
        """
        nodes shape: [batch_size, num_nodes, n_features]
        neighbours shape: [batch_size, num_nodes, 1]
        res [batch_size, num_nodes, n_features]
        """
        # return nodes.masked_fill(neighbours == 0, float('-inf'))
        return nodes.masked_fill(neighbours == 0, np.float32(-10e36))

    def reparametrize_n(self, mu, std, n=1):
        # reference :
        # http://pytorch.org/docs/0.3.1/_modules/torch/distributions.html#Distribution.sample_n
        def expand(v):
            if isinstance(v, Number):
                return torch.Tensor([v]).expand(n, 1)
            else:
                return v.expand(n, *v.size())

        if n != 1:
            mu = expand(mu)
            std = expand(std)

        eps = Variable(std.data.new(std.size()).normal_())
        return mu + eps * std
