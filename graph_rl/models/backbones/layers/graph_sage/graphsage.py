from typing import List

import torch as th
# from torch._C import float32
import torch.nn as nn
import numpy as np

from .aggregators import PoolingAggregator
from .encoder import Encoder


class GraphSage(nn.Module):
    """
    Класс GraphSAGE. Используется для получения эмбеддингов узлов графа.
    Возвраащет эмбеддинг для каждого из улов графа.
    """

    def __init__(self,
                 in_features: int,
                 aggregator_hidden_units: List[int],
                 encoder_out_features: List[int],
                 activation: str,
                 use_dropout: bool = True,
                 dropout_rate: float = 0.3,
                 device: str = 'cpu'):
        """
        :param in_features: Размерность входных данных.
        :type in_features: int
        :param aggregator_hidden_units: Размерности выходов аггрегаторов.
        :type aggregator_hidden_units: List[int]
        :param encoder_out_features: Размерности выходов энкодеров.
        :type encoder_out_features: List[int]
        :param activation: Название класса функции активации из torch.nn.
        :type activation: str
        :param use_dropout: Флаг использования Dropout-слоя между энкодерами.
        :type use_dropout: bool
        :param dropout_rate: Вероятность dropout.
        Релевантно при use_dropout = True
        :type dropout_rate: float
        :param device: Название девайса, на котором запущен модуль.
        :type device: str
        """
        nn.Module.__init__(self)
        self.in_features = in_features
        self.out_features = encoder_out_features[-1]
        self.device = device
        self.use_dropout = use_dropout
        if self.use_dropout:
            self.dropout_rate = dropout_rate
            self.dropout = nn.Dropout(p=self.dropout_rate)
        encoders = []
        for i in range(len(aggregator_hidden_units)):
            if i == 0:
                agg_in_features = in_features
            else:
                agg_in_features = encoder_out_features[i - 1]
            aggregator = PoolingAggregator(agg_in_features,
                                           aggregator_hidden_units[i],
                                           activation,
                                           self.device)
            encoder = Encoder(encoder_out_features[i],
                              aggregator,
                              activation,
                              device)
            encoders.append(encoder)
        self.encoders = nn.ModuleList(encoders)

    def forward(self,
                nodes: th.Tensor,
                adjacency_matrix: th.Tensor) -> th.Tensor:
        """
        :param nodes: Узлы графа.
        Размерность [batch, nodes_number, in_features].
        :type nodes: th.Tensor
        :param adjacency_matrix: Матрица смежности графа.
        Размерность [batch, nodes_number, nodes_number].
        :type adjacency_matrix: th.Tensor
        :return: Эмбеддинги узлов графа.
        Размерность [batch, nodes_number, out_features].
        :rtype: th.Tensor
        """
        batch_size = nodes.shape[0]
        nodes_number = nodes.shape[1]
        # Инициализация эмбеддингов значениями признаков
        embeddings = nodes
        # Обратить направление связей в графе
        adjacency_matrix = th.transpose(adjacency_matrix, 1, 2)  # Под вопросом
        for k in range(len(self.encoders)):
            out_features = self.encoders[k].out_features
            tmp = th.empty(size=(batch_size, nodes_number, out_features),
                           dtype=th.float,
                           device=self.device)
            for i in range(nodes_number):
                node = embeddings[:, i]
                neighbours_ids = adjacency_matrix[:, i].unsqueeze(-1)
                neighbour_nodes = self._get_neighbours(embeddings,
                                                       neighbours_ids)
                node = self.encoders[k](node, neighbour_nodes)
                tmp[:, i] = node
            embeddings = tmp
            embeddings = embeddings / th.norm(embeddings, dim=-1).unsqueeze(-1)
            if self.use_dropout and k < len(self.encoders) - 1:
                embeddings = self.dropout(embeddings)
            if th.isnan(embeddings).any():
                assert 'invalid embeddings'
            embeddings[th.isnan(embeddings)] = 0.0
        return embeddings

    def _get_neighbours(self,
                        nodes: th.Tensor,
                        adjacent_nodes: th.Tensor) -> th.Tensor:
        """
        Метод маскирования узлов на основании информации о их смежности с
        текущим узлом. Выбока невозможна, из-за конфликта размерностей числа
        соседей в батче.
        Для работы pooling-агрегатора маскирование
        производится значеним -inf.

        :param nodes: Узлы графа.
        Размерность [batch_size, nodes_number, in_features].
        :type nodes: th.Tensor
        :param adjacent_nodes: Вектор узлов, связанных с текущим узлом.
        Размерность [batch_size, nodes_number, 1].
        :type adjacent_nodes: th.Tensor
        :return: Узлы графа после маскирования.
        Размерность [batch_size, nodes_number, in_features].
        :rtype: th.Tensor
        """
        # return nodes.masked_fill(adjacent_nodes == 0, float('-inf'))
        return nodes.masked_fill(adjacent_nodes == 0, np.float32(-10e36))
