import torch as th
import torch.nn as nn


class PoolingAggregator(nn.Module):
    """
    Класс pooling-агрегатора GraphSAGE. Согласно оригинальной статье
    является лучшим из агрегаторов.
    Реализует операцию конкатенации агрегированных данных и исходным узлом
    для обратной совместимости кода энкодера.
    Входные данные о сосдених узлах должны быть маскированы значением -inf.
    Алгоритм работы:
    1) Пропустить данные о соседних узлах через полносвязный слой и
    нелинейность.
    2) Вычислить максимальные значения признаков среди соседейю
    3) Конкатенировать полученные признаки с признаками исходного узла.
    """

    def __init__(self,
                 in_features: int,
                 hidden_features: int,
                 activation: str,
                 device: str = 'cpu'):
        nn.Module.__init__(self)
        """
        :param in_features: Размерность входных данных.
        :type in_features: int
        :param hidden_features: Размерность полносвязного слоя.
        :type hidden_features: int
        :param activation: Название класса функции активации из torch.nn.
        :type activation: str
        :param device: Название девайса, на котором запущен модуль.
        :type device: str
        """
        self.in_features = in_features
        self.hidden_features = hidden_features
        self.out_features = in_features + hidden_features
        self.linear = nn.Linear(in_features, hidden_features, device=device)
        self.activation = getattr(nn, activation)()

    def forward(self,
                node: th.Tensor,
                node_neighbours: th.Tensor) -> th.Tensor:
        """
        :param node: Данные узла, для которого рассчитывается
        эмбеддинг. Размерность [batch_size, in_features].
        :type node: th.Tensor
        :param node_neighbours: Данные о соседних узлах.
        Размерность [batch_size, None, in_features]. None означает
        инвариантность к данной размерности.
        :type node_neighbours: th.Tensor
        :return: Агрегированные данные об узле и его соседях.
        Размерность [batch_size, out_features]
        :rtype: th.Tensor
        """
        # Размерность: [batch_size, None, hidden_features]
        hidden = self.activation(self.linear(node_neighbours))
        # Размерность: [batch_size, hidden_features]
        aggregated = hidden.max(dim=1).values
        # Размерность: [batch_size, out_features]
        #print('aggregated: ', aggregated)
        return th.cat([node, aggregated], dim=1)
