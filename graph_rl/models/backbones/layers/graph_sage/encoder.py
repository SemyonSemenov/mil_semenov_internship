import torch as th
import torch.nn as nn


class Encoder(nn.Module):
    """
    Класс энкодера GraphSAGE.
    Алгоритм работы:
    1) Агрегировать входные данные;
    2) Пропустить агрегированные данные через полносвязный слой и
    нелинейность.
    """

    def __init__(self,
                 out_features: int,
                 aggregator: nn.Module,
                 activation: str,
                 device: str = 'cpu'):
        """
        :param out_features: Размерность полносвязного слоя энкодера.
        :type out_features: int
        :param aggregator: Агрегатор узлов.
        :type aggregator: nn.Module
        :param activation: Название класса функции активации из torch.nn.
        :type activation: str
        :param device: Название девайса, на котором запущен модуль.
        :type device: str
        """
        super(Encoder, self).__init__()
        self.aggregator = aggregator
        self.linear = nn.Linear(aggregator.out_features,
                                out_features,
                                device=device)
        self.activation = getattr(nn, activation)()
        self.in_features = aggregator.in_features
        self.out_features = out_features

    def forward(self,
                node: th.Tensor,
                node_neighbours: th.Tensor) -> th.Tensor:
        """
        :param node: Данные узла, для которого рассчитывается
        эмбеддинг. Размерность [batch_size, in_features].
        :type node: th.Tensor
        :param node_neighbours: Данные о соседних узлах.
        Размерность [batch_size, None, in_features]. None означает
        инвариантность к данной размерности.
        :type node_neighbours: th.Tensor
        :return: Эмбеддинга узла. Размерность [batch_size, out_features]
        :rtype: th.Tensor
        """
        aggregated = self.aggregator(node, node_neighbours)
        return self.activation(self.linear(aggregated))
