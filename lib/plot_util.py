import sys

import gym
import numpy as np


import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append('..')

from lib.utils import check_make

sys.path.append('..')

time_path = '../time'

util_times = np.array([
    np.fromstring(i.replace('\n', '').replace('[', '').replace(']', ''), sep= ', ')
    for i in open(f'{time_path}/gpu_memoryUtil.txt', 'r').readlines()
])

print(len(util_times))



def build_util_plot(memory_util,
                    fig_save_path='../plots/',
                    name='memory_util'):
    check_make(fig_save_path)
    num_episodes = len(memory_util)
    fig, ax = plt.subplots(1, 1, figsize=(6, 4), dpi=200, sharex=True)
    plt.style.use('ggplot')
    ax.plot(np.arange(0, num_episodes, 1),
            [x[0] for x in memory_util],
            label='GPU1')
    ax.plot(np.arange(0, num_episodes, 1),
            [x[0] for x in memory_util],
            label='GPU2')

    ax.set_xlabel('Episodes')
    ax.set_ylabel('Memory usage (%)')
    ax.grid(True)
    ax.legend()
    fig.savefig(f'{fig_save_path}/{name}.png')


build_util_plot(util_times)
