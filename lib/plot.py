import argparse
import sys

import gym
import numpy as np
import ray
from ray.rllib.agents import ppo
from ray.rllib.models import ModelCatalog
from ray.tune.integration.wandb import WandbLogger
from ray.tune.logger import DEFAULT_LOGGERS

sys.path.append('..')
from analysis import build_time_plot
from data import Data
from environments import GraphEnv

from utils import Logger, check_make, fix_seeds, print_flags

time_path = '../time'

mean_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/mean_time.txt', 'r').readlines()
])
min_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/min_time.txt', 'r').readlines()
])
default_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/default_time.txt', 'r').readlines()
])

build_time_plot(mean_times, min_times, default_times, name='VGG16_default')