import cv2
import numpy as np

import time
from typing import List

import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from tensorflow.keras.regularizers import l2



def get_device_gpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def get_device_cpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def res_conv(x, s, filters, pt, inference_time, device_placement):
    f1, f2 = filters
    x_skip = x

    straight_layers = [
        layers.Conv2D(f1, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),
        layers.Activation(activation = 'relu'), 
        layers.Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),
        layers.Activation(activation = 'relu'),
        layers.Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),

    ]
    shortcut_layers = [
        layers.Conv2D(f2, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization()
    ]

    start = time.time()
    for i in range(len(straight_layers)):
        current_device = device_placement[pt]
        with tf.device(get_device_gpu(current_device)):
            x = straight_layers[i](x)
        pt += 1

    for i in range(len(shortcut_layers)):
        current_device = device_placement[pt]
        with tf.device(get_device_gpu(current_device)):
            x_skip = shortcut_layers[i](x_skip)
        pt += 1
    
    current_device = device_placement[pt]
    with tf.device(get_device_gpu(current_device)):
        current_device = device_placement[pt]
        x = layers.Add()([x, x_skip])
    pt += 1

    current_device = device_placement[pt]
    with tf.device(get_device_gpu(current_device)):
        current_device = device_placement[pt]
        x = layers.Activation(activation = 'relu')(x)
    pt += 1

    inference_time += (time.time() - start)
    
    return x, pt, inference_time

def res_identity(x, filters, pt, inference_time, device_placement):
    x_skip = x 
    f1, f2 = filters

    straight_layers = [
        layers.Conv2D(f1, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),
        layers.Activation(activation = 'relu'), 
        layers.Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),
        layers.Activation(activation = 'relu'),
        layers.Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001)),
        # layers.BatchNormalization(),
    ]

    start = time.time()
    for i in range(len(straight_layers)):
        current_device = device_placement[pt]
        with tf.device(get_device_gpu(current_device)):
            x = straight_layers[i](x)
        pt += 1

    current_device = device_placement[pt]
    with tf.device(get_device_gpu(current_device)):
        current_device = device_placement[pt]
        x = layers.Add()([x, x_skip])
    pt += 1

    current_device = device_placement[pt]
    with tf.device(get_device_gpu(current_device)):
        current_device = device_placement[pt]
        x = layers.Activation(activation = 'relu')(x)
    pt += 1

    inference_time += (time.time() - start)

    return x, pt, inference_time


def get_reward(device_placement: List) -> int:
    (train_images, train_labels), (test_images,
                                   test_labels) = datasets.cifar10.load_data()
    train_images, test_images = train_images / 255.0, test_images / 255.0
    x = np.array([cv2.resize(src = train_images[np.random.choice(np.arange(train_images.shape[0]))], dsize = (224, 224)) for i in range(1)])
    
    first_stage = [
        layers.ZeroPadding2D(padding=(3, 3)),
        layers.Conv2D(64, kernel_size=(7, 7), strides=(2, 2)),
        # layers.BatchNormalization(),
        layers.Activation(activation = 'relu'),
        layers.MaxPooling2D((3, 3), strides=(2, 2))
    ]
    pt = 0
    inference_time = 0.0

    start = time.time()
    for i in range(len(first_stage)):
        current_device = device_placement[pt]
        with tf.device(get_device_gpu(current_device)):
            x = first_stage[i](x)
        pt += 1

    inference_time += (time.time() - start)

    x, pt, inference_time = res_conv(x, s=1, filters=(64, 256), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(64, 256), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(64, 256), pt=pt, inference_time=inference_time, device_placement=device_placement)

    x, pt, inference_time = res_conv(x, s=2, filters=(128, 512), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(128, 512), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(128, 512), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(128, 512), pt=pt, inference_time=inference_time, device_placement=device_placement)

    x, pt, inference_time = res_conv(x, s=2, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(256, 1024), pt=pt, inference_time=inference_time, device_placement=device_placement)

    x, pt, inference_time = res_conv(x, s=2, filters=(512, 2048), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(512, 2048), pt=pt, inference_time=inference_time, device_placement=device_placement)
    x, pt, inference_time = res_identity(x, filters=(512, 2048), pt=pt, inference_time=inference_time, device_placement=device_placement)


    last_stage = [
        layers.AveragePooling2D((2, 2), padding='same'),
        #layers.Flatten(),
        #layers.Dense(1000, activation='softmax', kernel_initializer='he_normal')
    ]

    start = time.time()
    for i in range(len(last_stage)):
        current_device = device_placement[pt]
        with tf.device(get_device_gpu(current_device)):
            x = last_stage[i](x)
        pt += 1

    inference_time += (time.time() - start)

    return inference_time