import time
from typing import List

import tensorflow as tf
from tensorflow.keras import datasets, layers, models


def get_device_gpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def get_device_cpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def get_reward(device_placement: List) -> int:
    tf.debugging.set_log_device_placement(True)
    (train_images, train_labels), (test_images,
                                   test_labels) = datasets.cifar10.load_data()
    train_images, test_images = train_images / 255.0, test_images / 255.0
    # previous_model
    model_layers = [
        layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.MaxPooling2D((2, 2)),
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.Flatten(),
        layers.Dense(64, activation='relu'),
        layers.Dense(10)
    ]

    assert len(model_layers) == len(device_placement)

    out = train_images[:100]

    start = time.time()
    for i in range(len(model_layers)):
        current_device = device_placement[i]
        with tf.device(get_device_gpu(current_device)):
            out = model_layers[i](out)
    inference_time = time.time() - start

    return inference_time
