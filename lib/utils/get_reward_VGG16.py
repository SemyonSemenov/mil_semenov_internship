import cv2
import numpy as np

import time
from typing import List

import tensorflow as tf
from tensorflow.keras import datasets, layers, models


def get_device_gpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def get_device_cpu(num: int) -> str:
    return '/GPU:{}'.format(num)


def get_reward(device_placement: List) -> int:
    tf.debugging.set_log_device_placement(True)
    (train_images, train_labels), (test_images,
                                   test_labels) = datasets.cifar10.load_data()
    train_images, test_images = train_images / 255.0, test_images / 255.0
    # previous_model
    model_layers = [
        layers.Conv2D(input_shape=(224,224,3), filters=64, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=64,kernel_size=(3,3),padding="same", activation="relu"),
        layers.MaxPool2D(pool_size=(2,2), strides=(2,2)),
        layers.Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"),
        layers.MaxPool2D(pool_size=(2,2),strides=(2,2)),
        layers.Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"),
        layers.MaxPool2D(pool_size=(2,2),strides=(2,2)),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.MaxPool2D(pool_size=(2,2),strides=(2,2)),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"),
        layers.MaxPool2D(pool_size=(2,2),strides=(2,2)),
        layers.Flatten(),
        layers.Dense(units=4096,activation="relu"),
        layers.Dense(units=4096,activation="relu"),
        layers.Dense(units=10, activation="softmax")
    ]

    assert len(model_layers) == len(device_placement)

    amount = 100
    out = np.array([cv2.resize(src = train_images[i], dsize = (224, 224)) for i in range(amount)])

    start = time.time()
    for i in range(len(model_layers)):
        current_device = device_placement[i]
        with tf.device(get_device_gpu(current_device)):
            out = model_layers[i](out)
    inference_time = time.time() - start

    return inference_time
