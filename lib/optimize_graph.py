import argparse
import sys

import gym
import numpy as np
import ray
from ray.rllib.agents import ppo
from ray.rllib.models import ModelCatalog
from ray.tune.integration.wandb import WandbLogger
from ray.tune.logger import DEFAULT_LOGGERS

sys.path.append('..')
from analysis import build_time_plot
from data import Data
from environments import GraphEnv

from utils import Logger, check_make, fix_seeds, print_flags
# from models import CustomTorchModel
# from callbacks import MyCallbacks

fix_seeds()

parser = argparse.ArgumentParser()
parser.add_argument('-n',
                    '--name',
                    default='tmp',
                    help='name of this experiment')
parser.add_argument('-e',
                    '--episodes',
                    type=int,
                    default=100,
                    help='number of eepisode to train the agent')
parser.add_argument('-wt',
                    '--wandb_tags',
                    type=str,
                    default=None,
                    help='comma separated W&B tags of the experiment')
parser.add_argument('--nolog',
                    action='store_true',
                    help='turn off wandb logging')
args = parser.parse_args()
print_flags(args)

# logger = Logger(args.name if not args.nolog else None)
# ModelCatalog.register_custom_model("my_torch_model", CustomTorchModel)

trainer_config = ppo.DEFAULT_CONFIG.copy()
trainer_config['num_workers'] = 1
trainer_config["train_batch_size"] = 1 #400
trainer_config["sgd_minibatch_size"] = 1 #64
trainer_config["num_sgd_iter"] = 1
trainer_config["num_gpus"] = 4
trainer_config["rollout_fragment_length"] = 1
trainer_config["batch_mode"] = "complete_episodes"
trainer_config["num_envs_per_worker"] = 1
trainer_config["framework"] = "torch"
trainer_config["env"] = GraphEnv
#trainer_config['log_level'] = 'ERROR'

# trainer_config['model'] = {"custom_model": "my_torch_model"}
# trainer_config["logger_config"] = {
#     "wandb": {
#         "project": "graph_optim",
#         "api_key_file": "./wandb_api_key_file",
#         "log_config": True
#     }
# }
# trainer_config["callbacks"] = MyCallbacks

ray.init()
#ray.init(log_to_driver=False)
#disable warnings

time_path = '../time'
check_make(time_path)
open(f'{time_path}/mean_time.txt', 'w').close()  # make a time file being empty in the beginning of training
open(f'{time_path}/min_time.txt', 'w').close()
open(f'{time_path}/default_time.txt', 'w').close()
open(f'{time_path}/gpu_memoryUtil.txt', 'w').close()


def train(num_episodes, trainer_config, save_path):
    check_make(save_path)
    stat_file = open(f'{save_path}/results.txt', 'w')
    for i in range(num_episodes):
        print(f'Num episode - {i}')
        result = ray.tune.run(
            ppo.PPOTrainer,
            checkpoint_freq=1,
            config=trainer_config,
            stop={"training_iteration": 1},
            # loggers=DEFAULT_LOGGERS + (WandbLogger, )
        )
        stat_file.write(f'Episode {i}:\n')
        for key, value in result.results_df.items():
            stat_file.write(f'{key}: {value}\n')
        stat_file.write('\n\n')
        print('Result: %s\n\n' % result.results_df['episode_reward_max'].to_numpy()[0])
    stat_file.close()

train(args.episodes, trainer_config, save_path='../results')

mean_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/mean_time.txt', 'r').readlines()
])
min_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/min_time.txt', 'r').readlines()
])
default_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/default_time.txt', 'r').readlines()
])

build_time_plot(mean_times, min_times, default_times, name='ResNet50')