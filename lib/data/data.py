import numpy as np

from .model_info_VGG16 import adj_matrix, model_dict

1
# Data Storage
class Data():
    def __init__(self):
        self.adj = adj_matrix
        self.data = model_dict

        self.NUM_DEVICES = 4
        self.NUM_NODES = len(adj_matrix)
        self.NUM_OPS = max([x['op_type'] for x in model_dict]) + 1
        self.MAX_IN_SHAPE = np.array([
            max([x['input_shape'][i] for x in model_dict])
            for i in range(len(model_dict[0]['input_shape']))
        ],
                                     dtype=np.int64)
        self.MAX_OUT_SHAPE = np.array([
            max([x['output_shape'][i] for x in model_dict])
            for i in range(len(model_dict[0]['input_shape']))
        ],
                                      dtype=np.int64)
