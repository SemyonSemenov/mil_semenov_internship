adj_matrix = [[0, 1, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0],
              [0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 1, 0, 0, 0],
              [0, 0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0],
              [0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0, 0]]

model_dict = [
    {
        'op_type': 0,  #[1, 0, 0, 0],
        'input_shape': [32, 32, 3],
        'output_shape': [30, 30, 32],
    },
    {
        'op_type': 2,  #[0, 0, 1, 0],
        'input_shape': [30, 30, 32],
        'output_shape': [15, 15, 32],
    },
    {
        'op_type': 0,  #[1, 0, 0, 0],
        'input_shape': [15, 15, 32],
        'output_shape': [13, 13, 64],
    },
    {
        'op_type': 2,  #[0, 0, 1, 0],
        'input_shape': [13, 13, 64],
        'output_shape': [6, 6, 64],
    },
    {
        'op_type': 0,  #[1, 0, 0, 0],
        'input_shape': [6, 6, 64],
        'output_shape': [4, 4, 64],
    },
    {
        'op_type': 3,  #[0, 0, 0, 1],
        'input_shape': [4, 4, 64],
        'output_shape': [1024, 1, 1],
    },
    {
        'op_type': 1,  #[0, 1, 0, 0],
        'input_shape': [1024, 1, 1],
        'output_shape': [64, 1, 1],
    },
    {
        'op_type': 1,  #[0, 1, 0, 0],
        'input_shape': [64, 1, 1],
        'output_shape': [10, 1, 1],
    },
]
