import os
import torch
import json
import numpy as np
from collections import OrderedDict


def list_of_dicts(valid_keys, name):
    new_model_dict = OrderedDict()

    json_file = open(f"/home/semenov/Projects/huawei_dnn_graph_optimisers/nets/{name}.json")
    loaded = json.load(json_file)
    model_dict = loaded['model_dict']
    adj_matrix = loaded['adj_matrix']

    new_model_dict = OrderedDict()
    for k in valid_keys:
        new_model_dict[k] = np.array(
            [model_dict[i][k] for i in range(len(model_dict))])

    return np.array(adj_matrix), new_model_dict

# Data Storage
class Data():
    def __init__(self):
        self.adj_matrix, self.data  = list_of_dicts(['op_type', 'input_shape', 'output_shape'], "resnet50_beheaded")
        self.processing()

        self.NUM_DEVICES = 4
        self.NUM_NODES = len(self.adj_matrix)

        self.MAX_IN_SHAPE = np.array([
            max([x[i] for x in self.data['input_shape']])
            for i in range(len(self.data['input_shape'][0]))
        ],
                                     dtype=np.int64)

        self.MAX_OUT_SHAPE = np.array([
            max([x[i] for x in self.data['output_shape']])
            for i in range(len(self.data['output_shape'][0]))
        ],
                                     dtype=np.int64)

        self.NUM_OPS = max([self.data['op_type'][i] for i in range(len(self.data['op_type']))]) + 1

    def shaping(self, type : str):
        assert type == 'input_shape' or type == 'output_shape'
        L = max(len(self.data[type][i]) for i in range(len(self.data[type])))

        for i, row in enumerate(self.data[type]):
            for j, elem in enumerate(row):
                if elem == 0:
                    self.data[type][i][j] = 1
            if len(row) != L:
                self.data[type][i] += [1] * (L - len(self.data[type][i]))

    def processing(self):
        self.shaping('input_shape')
        self.shaping('output_shape')
