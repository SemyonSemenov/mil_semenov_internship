import sys
import gym
import numpy as np
import nvsmi

sys.path.append('..')
from ray.tune.integration.wandb import wandb_mixin

from lib.data import Data
from lib.utils import check_make, get_reward


# Environment
class GraphEnv(gym.Env, Data):
    def __init__(self, env_config, logger=None):
        Data.__init__(self)
        self.logger = logger
        self.shape_dims = len(self.MAX_IN_SHAPE)
        self.small_reward = env_config.get("small", 0)
        self.large_reward = env_config.get("large", 10)
        self._horizon = 10
        self._counter = 0  # For terminating the episode
        self._setup_spaces()
        self.save_path = '../time'
        self.reset()
        self.placement = []
    def _setup_spaces(self):
        spaces = {
            'op_type':
            gym.spaces.MultiDiscrete([self.NUM_OPS] * self.NUM_NODES),
            'input_shape':
            gym.spaces.Box(low=1,
                           high=max(self.MAX_IN_SHAPE),
                           shape=(self.NUM_NODES, self.shape_dims),
                           dtype=np.int64),
            'output_shape':
            gym.spaces.Box(low=1,
                           high=max(self.MAX_OUT_SHAPE),
                           shape=(self.NUM_NODES, self.shape_dims),
                           dtype=np.int64),
            'gpu_num_nodes':
            gym.spaces.MultiDiscrete([self.NUM_NODES] * self.NUM_DEVICES),
            'gpu_memoryUtil':
            gym.spaces.Box(low=0.0,
                           high=1.0,
                           shape=(self.NUM_DEVICES, 1),
                           dtype=np.float64),
            'gpu_memoryFree':
            gym.spaces.Box(low=0.0,
                           high=11019.0,
                           shape=(self.NUM_DEVICES, 1),
                           dtype=np.float64),
            'gpu_load':
            gym.spaces.Box(low=0.0,
                           high=1.0,
                           shape=(self.NUM_DEVICES, 1),
                           dtype=np.float64),
        }
        self.observation_space = gym.spaces.Dict(spaces)
        self.action_space = gym.spaces.Discrete(self.NUM_DEVICES)

    def reset(self):
        self.default_placement = np.ones(self.NUM_NODES, dtype=int)
        self.best_runtime = get_reward(self.default_placement)

        self.state = self.data

        self.state['gpu_num_nodes'] = np.array([0] * self.NUM_DEVICES)
        self.state['gpu_memoryUtil'] = np.zeros((self.NUM_DEVICES, 1))
        self.state['gpu_memoryFree'] = np.zeros((self.NUM_DEVICES, 1))
        self.state['gpu_load'] = np.zeros((self.NUM_DEVICES, 1))

        for i, gpu in enumerate(nvsmi.get_gpus()):
            self.state['gpu_memoryUtil'][i][0] = np.float64(gpu.mem_util / 100.0)
            self.state['gpu_memoryFree'][i][0] = np.float64(gpu.mem_free)
            self.state['gpu_load'][i][0] = np.float64(gpu.gpu_util / 100.0)
        
        self._counter = 0
        self.times = []
        check_make(self.save_path)
        return self.state

    def step(self, action):
        reward = 0
        runtime = -1
        done = False
        
        self.state['gpu_num_nodes'][action] += 1
        self.placement += [action]

        for i, gpu in enumerate(nvsmi.get_gpus()):
            self.state['gpu_memoryUtil'][i][0] = np.float64(gpu.mem_util / 100.0)
            self.state['gpu_memoryFree'][i][0] = np.float64(gpu.mem_free)
            self.state['gpu_load'][i][0] = np.float64(gpu.gpu_util / 100.0)
      
        if len(self.placement) == self.NUM_NODES:
            runtime = get_reward(self.placement)
            reward = self.best_runtime - runtime
            if runtime < self.best_runtime:
                self.best_runtime = runtime

            print('Iter: ', self._counter)
            print('Runtime: ', runtime)
            print('Reward: ', reward)

            print('gpu_num_nodes:', self.state['gpu_num_nodes'])
            print('gpu_memoryUtil: ', self.state['gpu_memoryUtil'].T)
            print('gpu_memoryFree: ', self.state['gpu_memoryFree'].T)
            print('gpu_load: ', self.state['gpu_load'].T)
            print('\n')

            self.placement = []
            self.state['gpu_num_nodes'] = np.array([0] * self.NUM_DEVICES)
            self._counter += 1

            if self._counter >= self._horizon:
                done = True
                with open(f'{self.save_path}/mean_time.txt', 'a') as f:
                    f.write(str(np.mean(self.times)) + '\n')
                    f.close()
                with open(f'{self.save_path}/min_time.txt', 'a') as f:
                    f.write(str(np.min(self.times)) + '\n')
                    f.close()
                with open(f'{self.save_path}/default_time.txt', 'a') as f:
                    f.write(str(get_reward(self.default_placement)) + '\n')
                    f.close()
                with open(f'{self.save_path}/gpu_memoryUtil.txt', 'a') as f:
                    f.write(str([x[0] for x in self.state['gpu_memoryUtil']]) + '\n')
                    f.close()
                
            else:
                done = False
                self.times.append(runtime)

        return self.state, reward, done, {}