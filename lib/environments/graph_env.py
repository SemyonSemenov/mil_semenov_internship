import sys

import gym
import numpy as np

sys.path.append('..')
from ray.tune.integration.wandb import wandb_mixin

from lib.data import Data
from lib.utils import check_make, get_reward, invert_list_of_dicts


# Environment
class GraphEnv(gym.Env, Data):
    def __init__(self, env_config, logger=None):
        Data.__init__(self)
        self.logger = logger
        self.shape_dims = len(self.MAX_IN_SHAPE)
        self.small_reward = env_config.get("small", 0)
        self.large_reward = env_config.get("large", 10)
        self._horizon = 10
        self._counter = 0  # For terminating the episode
        self._setup_spaces()
        self.save_path = '../time'
        self.reset()

    def _setup_spaces(self):
        spaces = {
            'op_type':
            gym.spaces.MultiDiscrete([self.NUM_OPS] * self.NUM_NODES),
            'input_shape':
            gym.spaces.Box(low=1,
                           high=max(self.MAX_IN_SHAPE),
                           shape=(self.NUM_NODES, self.shape_dims),
                           dtype=np.int64),
            'output_shape':
            gym.spaces.Box(low=1,
                           high=max(self.MAX_OUT_SHAPE),
                           shape=(self.NUM_NODES, self.shape_dims),
                           dtype=np.int64)
        }
        self.observation_space = gym.spaces.Dict(spaces)
        self.action_space = gym.spaces.MultiDiscrete([self.NUM_DEVICES] *
                                                     self.NUM_NODES)

    def reset(self):
        self.default_placement = np.ones(self.NUM_NODES, dtype=int)
        self.best_runtime = get_reward(self.default_placement)
        self.state = invert_list_of_dicts(
            self.data, keys=self.observation_space.sample().keys())
        self._counter = 0
        self.times = []
        check_make(self.save_path)
        return self.state

    def step(self, action):
        runtime = get_reward(action)
        reward = self.best_runtime - runtime
        if runtime < self.best_runtime:
            self.best_runtime = runtime
        self._counter += 1
        print(f'Iter {self._counter}')
        print(f'Runtime - {runtime};\nBest runtime: {self.best_runtime}')
        print(f'Reward: {reward}\nDevice placement: {action}')
        print(f'State: {self.state}\n\n')

        if self._counter >= self._horizon:
            done = True
            with open(f'{self.save_path}/mean_time.txt', 'a') as f:
                f.write(str(np.mean(self.times)) + '\n')
                f.close()
            with open(f'{self.save_path}/min_time.txt', 'a') as f:
                f.write(str(np.min(self.times)) + '\n')
                f.close()
            with open(f'{self.save_path}/default_time.txt', 'a') as f:
                f.write(str(get_reward(self.default_placement)) + '\n')
                f.close()
        else:
            done = False
            self.times.append(runtime)
        return self.state, reward, done, {}