# Internship

## First part

The current intern branch with the RLlib and stable baselines 3 frameworks.


### lib/data

- `lib/data` contains class `Data()` in `custom_data.py` which requires file `nets/your_network.json` to define `adj_matrix` and `data`.

- `model_info_VGG16.py` or `model_info.py` can be used instead of `custom_data.py` in `data.py`.

### lib/environments

- `graph_env.py` contains default graph environment with `spaces = {op_type, input_shape, output_shape}`.

- `graph_env_utilization.py` contains an environment that allows to perform placement sequentially and controls the number of placed nodes on the appropriate device.

- `graph_env_sequential.py` in addition to the `graph_env_utilization.py`, not only performs placement sequentially, but also takes intermediate placement into account in the observation space.

- `graph_env_nvsmi.py` contains all features above with memory utilization.

### lib/utils

- `graph_env.py` calculates the runtime for default network.

- `get_reward_VGG16.py` calculates the runtime for VGG16.

- `get_reward_ResNet50.py` calculates the runtime for ResNet50.


### nets

Contains networks to evaluate in `.json` format.

## Second part

To run the project, please edit main.py's main function and run it.

Contains:

1) GraphSAGE.

2) Agent with Dropout-layer.

3) Environment:

   * Observations: whole graph information;
   
   * Reward: default_runtime - runtime;
   
4) A custom ActorCriticPolicy class to work with our MlpExtractor 
(agent neural network).

5) A custom FeatureExtractor class which translates input data to 
output (mandatory in stable_baselines3).

6) Configuration files:

    * Environments;
   
    * Training;
   
    * MlpExtractor.

