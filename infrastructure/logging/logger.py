import wandb


class Logger:
    def __init__(self, run_name, wandb_on=True, tags=None):
        self.wandb_on = (run_name is not None) and wandb_on
        if self.wandb_on:
            if tags is not None:
                tags = tags.replace(' ', '').split(',')
                wandb.init(project="graph_optim", name=run_name, tags=tags)
            else:
                wandb.init(project="graph_optim", name=run_name)
            self.path = wandb.run.dir
            wandb.log({'path': self.path})
        else:
            self.path = run_name
        self.x_train = 0
        self.x_valid = 0

    def __bool__(self):
        return (self.path is not None)

    def train(self, train_mode):
        self.train_mode = train_mode

    def _log_train(self, loss):
        self.log({"train_loss": loss, "train_axis": self.x_train})
        self.x_train += 1

    def _log_valid(self, loss):
        self.log({"valid_loss": loss, "valid_axis": self.x_valid})
        self.x_valid += 1

    def log_loss(self, loss):
        if self.train_mode:
            self._log_train(loss)
        else:
            self._log_valid(loss)

    def log(self, logs_dict):
        if self.wandb_on:
            wandb.log(logs_dict)
