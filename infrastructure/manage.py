from infrastructure.cli import CLI

if __name__ == "__main__":
    cli_instance = CLI()
    cli_instance.execute()
