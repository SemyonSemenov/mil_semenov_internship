"""
Command-line interface.
"""

import argparse


class CLI:
    def __init__(self):
        self._parser = argparse.ArgumentParser(
            description="CLI"
        )

        self._parser.add_argument("pipeline",
                                  metavar="p",
                                  type=str,
                                  nargs=1,
                                  help="Pipeline to execute")

    def execute(self):
        args = self._parser.parse_args()
        print(args.pipeline)
