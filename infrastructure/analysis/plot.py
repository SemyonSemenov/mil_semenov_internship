import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append('..')

from infrastructure.utils import check_make


def build_time_plot(mean_times,
                    min_times,
                    default_times,
                    fig_save_path='../plots/',
                    name='inference_time_comparison'):
    check_make(fig_save_path)
    num_episodes = len(mean_times)
    fig, ax = plt.subplots(1, 1, figsize=(6, 4), dpi=200, sharex=True)
    plt.style.use('ggplot')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * mean_times,
            label='mean optimized placement runtime')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * min_times,
            label='min optimized placement runtime')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * default_times,
            label='default placement runtime')

    ax.set_xlabel('Episodes')
    ax.set_ylabel('Runtime (ms)')
    ax.grid(True)
    ax.legend()
    fig.savefig(f'{fig_save_path}/{name}.png')
