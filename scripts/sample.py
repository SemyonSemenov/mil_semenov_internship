from graph_rl.environments import GraphEnv
from graph_rl.utils import fix_seeds


def sample():
    fix_seeds()

    env = GraphEnv()

    episode_reward = 0
    done = False
    obs = env.reset()
    # print(obs)

    while not done:
        action = env.action_space.sample()
        # print(action)
        obs, reward, done, info = env.step(action)
        print(reward)
        episode_reward += reward
        # break

    print(episode_reward)
