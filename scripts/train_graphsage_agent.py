"""
The main file for starting the training process.
"""

import os

from stable_baselines3.common.logger import configure

from configs import environment_config, train_config
from graph_rl import environments
from graph_rl.models.backbones.custom_stable_baselines3 import callbacks, \
    features_extractors, policy, algorithms
from graph_rl.utils import get_reward


def check_make(path):
    if not os.path.exists(path):
        os.mkdir(path)


def train_graphsage_agent():
    time_path = '/home/semenov/Projects/huawei_dnn_graph_optimisers/time/'
    check_make(time_path)
    open(f'{time_path}/mean_time.txt', 'w').close()  # make a time file being empty in the beginning of training
    open(f'{time_path}/min_time.txt', 'w').close()
    open(f'{time_path}/default_time.txt', 'w').close()

    
    environment_class = getattr(environments, train_config['environment'])
    environment = environment_class(config=environment_config)

    # Create a folder in which the experiment logs are stored.
    if not os.path.exists(train_config['log_path']):
        os.mkdir(train_config['log_path'])
    logger = configure(train_config['log_path'], ["stdout", "json", "log"])

    if train_config['callback'] is not None:
        callback_class = getattr(callbacks, train_config['callback'])
        callback = callback_class(**train_config['callback_kwargs'])
    else:
        callback = None

    features_extractor_class = getattr(features_extractors,
                                       train_config[
                                           'features_extractor_class'])
    features_extractor_kwargs = train_config['features_extractor_kwargs']

    # We use the custom Policy class to use its mlp_extractors
    # (agent model).
    policy_kwargs = {
        'features_extractor_class': features_extractor_class,
        'features_extractor_kwargs': features_extractor_kwargs,
    }
    model = algorithms.ErrorHandlingPPO(n_steps=train_config['n_steps'],
                                        batch_size=train_config['batch_size'],
                                        policy=policy.CustomActorCriticPolicy,
                                        env=environment,
                                        policy_kwargs=policy_kwargs,
                                        device=train_config['device'],
                                        verbose=1)
    model.set_logger(logger)
    model.learn(train_config['batch_size'] * train_config['n_iter'],
                callback=callback)
    model.save(train_config['agent_weights_path'])

    # Get the model runtime on the trained agent.
    observation = environment.reset()
    action, _ = model.predict(observation, deterministic=True)
    print(f'Graph is executed in {get_reward(action)} ms')
    
