import numpy as np

import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append('..')



def build_time_plot(mean_times,
                    min_times,
                    default_times,
                    fig_save_path='/home/semenov/Projects/huawei_dnn_graph_optimisers/plots/',
                    name='inference_time_comparison'):
    num_episodes = len(mean_times)
    fig, ax = plt.subplots(1, 1, figsize=(6, 4), dpi=200, sharex=True)
    plt.style.use('ggplot')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * mean_times,
            label='mean optimized placement runtime')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * min_times,
            label='min optimized placement runtime')
    ax.plot(np.arange(0, num_episodes, 1),
            1000 * default_times,
            label='default placement runtime')

    ax.set_xlabel('Episodes')
    ax.set_ylabel('Runtime (ms)')
    plt.title(name)
    ax.grid(True)
    ax.legend()
    fig.savefig(f'{fig_save_path}/{name}.png')


time_path = '/home/semenov/Projects/huawei_dnn_graph_optimisers/time/'
mean_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/mean_time.txt', 'r').readlines()
])
min_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/min_time.txt', 'r').readlines()
])
default_times = np.array([
    float(i.replace('\n', ''))
    for i in open(f'{time_path}/default_time.txt', 'r').readlines()
])

cut = 5
build_time_plot(mean_times[cut:], min_times[cut:], default_times[cut:], name='GraphSAGE Sampling + XLNet')

